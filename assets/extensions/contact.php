<?php 

/* --------------------------------------------------------------

  @name     contact.php
  @desc     Custom class for Open Font Library. Handles Member Contact.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

/* Requires
-------------------------------------------------------------- */
require_once("../../aiki.php");


/* Globals
-------------------------------------------------------------- */
global $membership;
global $config;


/* Constants
-------------------------------------------------------------- */
$userid = $membership->userid;
$username = $membership->username;
$usergroup = $membership->group_level;

/* Function
-------------------------------------------------------------- */

/**
 * Only allow authenticated Aiki Users to access this script.
 */
	 
if(!$userid || $usergroup > 5) { die('You do not have permission to access this script.'); }

$output_heading = "";
$output_body = "";

if (isset($_POST["subject"]) && isset($_POST['message']) && isset($_POST['recipient_id'])) {

  $subject = $_POST["subject"];
  $message = $_POST['message'];
  $recipient_id = $_POST['recipient_id'];

  $email = $db->get_var("SELECT email FROM aiki_users WHERE userid = $recipient_id");

  $emailfrom = $db->get_var("SELECT email FROM aiki_users WHERE userid = $membership->userid");
  
  if($email) {

    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";
    $headers .= "From: $emailfrom\r\n";

    $message = $message."<br/><br/>--<br/>This message was sent by the <a href=\"" . $config['url'] . "\">Open Font Library</a>.<br/>You can visit $username's <a href=\"" . $config['url'] . "contact/$username\">contact page</a> or reply to this email.";

    mail($email,$subject,$message,$headers);
			    
    $output_heading = "Message sent";
    $output_body = "Your message was sent to $username.";
  } else {
    $output_heading = "Message not sent";
    $output_body = "We can't seem to find contact information for this user.";
  }
			  
} else {
  $output_heading = "Error";
  $output_body = "You need to supply a subject and message.";
}

$html = "<div class=\"element\"><ul class=\"primary\"><li class=\"heading first\"><a>" . $output_heading . "</a></li></ul><div class=\"spec\"></div><div class=\"details\"><dl class=\"info\">" . $output_body . "</div></div>";

echo $html;

?>


