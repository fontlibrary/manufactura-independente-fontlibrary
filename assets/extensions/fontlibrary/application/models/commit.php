<?php 

/** --------------------------------------------------------------

  @name     commit.php
  @desc     Commit class for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class Commit extends Model
{
  protected $family_id;
  protected $hash;
  protected $commit_date;
  protected $download;

  function __construct($commit_id = NULL)
  {
    $this->table = 'oflb_commits';
    if($commit_id != NULL)
    {
      parent::__construct($commit_id);
    }
    
  }
  
  function create($family_id, $hash, $commit_date, $download)
  {
    $this->family_id = $family_id;
    $this->hash = $hash;
    $this->commit_date = $commit_date;
    $this->download = $download;
  }
  
  function save()
  {
    global $db;
    $db->query("INSERT into oflb_commits (family_id, hash, commit_date, download) VALUES ('$this->family_id', '$this->hash', FROM_UNIXTIME($this->commit_date), '$this->download')");
    $this->id = mysql_insert_id();
    return mysql_insert_id();
  }
      
}
  
?>
