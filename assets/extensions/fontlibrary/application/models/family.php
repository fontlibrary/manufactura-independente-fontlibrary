<?php 

/** --------------------------------------------------------------

  @name     family.php
  @desc     Family class for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class Family extends Model
{

  protected $contributor_id;
  protected $family_name;
  protected $project_name;
  protected $created_at;
  protected $description;
  protected $category_id;
  protected $license_id;
  protected $status;
  
  protected $sample;

  function __construct($family_id = NULL)
  {
    $this->table = 'oflb_families';
    if($family_id != NULL)
    {
      parent::__construct($family_id);
    }
  }
  
  function create($family_name, $description, $contributor_id, $category_id)
  {
    $this->contributor_id = $contributor_id;
    $this->family_name = $family_name;
    $this->description = $description;
    $this->category_id = $category_id;
    $this->project_name = $this->generate_name($family_name);
    $this->created_at = time();
  }
  
  function load()
  {
    global $db;
    
    $results = $this->select();
    $family = $results[0];
    $this->contributor_id = $family->contributor_id;
    $this->family_name = $family->family_name;
    $this->project_name = $family->project_name;
    $this->created_at = $family->created_at;
    $this->description = $family->description;
    $this->category_id = $family->category_id;
    $this->license_id = $family->license_id;
    $this->status = $family->status;
    $this->sample = $db->get_var("SELECT sample FROM oflb_fonts WHERE family_id = '$this->id' LIMIT 1");
  }
  
  function save()
  {
    global $db;
    if(isset($this->id))
    {
      $db->query("UPDATE $this->table SET contributor_id = '$this->contributor_id', family_name = '$this->family_name', description = '$this->description', category_id = '$this->category_id', status = '$this->status' WHERE id = '$this->id'");
      $db->query("UPDATE oflb_fonts SET sample = '$this->sample' WHERE family_id = '$this->id'");
    }
    else
    {
      $db->query("INSERT into oflb_families (contributor_id, family_name, project_name, created_at, description, category_id, status) VALUES ('$this->contributor_id', '$this->family_name', '$this->project_name', FROM_UNIXTIME($this->created_at), '$this->description', '$this->category_id', '$this->status')");
      
      $this->id = mysql_insert_id();
      return mysql_insert_id();
    }
  }
  
  function set_contributor($user)
  {
    global $db;
    if($user)
    { 
        $this->contributor_id = $user->get_id();
    }
    else
    {
        $this->contributor_id = '';
    }
  }
  
  function set_category_id($category_id)
  {
    $this->category_id = $category_id;
  }
  
  function set_family_name($family_name)
  {
    $this->family_name = $family_name;
  }
  
  function set_description($description)
  {
    $this->description = $description;
  }
  
  function set_status($status)
  {
  	$this->status = $status;
  }
  
  function set_sample($sample)
  {
    $this->sample = $sample;
  }
  
  function get_contributor_id()
  {
    return $this->contributor_id;
  }
  
  function get_family_name()
  {
    return $this->family_name;
  }
  
  function get_project_name()
  {
    return $this->project_name;
  }
  
  function get_created_at()
  {
    return $this->created_at;
  }
  
  function get_sample()
  {
    return $this->sample;
  }
  
  function update_license()
  {
    global $db;
    $db->query("UPDATE oflb_families SET license_id = (SELECT id from oflb_licenses WHERE license = (SELECT license FROM oflb_fonts WHERE family_id = '$this->id' ORDER BY count(license) LIMIT 1)) WHERE id = '$this->id'");
  }
  
  function name_taken($family_name)
  {
    global $db;
    $project_name = $this->generate_name($family_name);
    return $db->get_var("SELECT id FROM $this->table WHERE project_name = '$project_name' AND id != '$this->id'");
  }
  
  protected function generate_name($family_name)
  {
    $text = strtolower($family_name);
    $text = preg_replace('/[^\\pL0-9]+/u', '-', $text);
    $text = trim($text, "-");
    $text = iconv("utf-8", "us-ascii//TRANSLIT", $text);
    $text = preg_replace('/[^-a-z0-9]+/i', '', $text);
    
    return $text;
  }

}

?>
