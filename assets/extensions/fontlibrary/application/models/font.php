<?php 

/** --------------------------------------------------------------

  @name     font.php
  @desc     Font class for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class Font extends Model
{
  protected $commit_id;
  protected $family_id;
  protected $font_hash;
  protected $file_directory;
  protected $file_name;
  protected $file_extension;
  protected $fontface_family;
  protected $sample;
  
  protected $common_name;
  protected $native_name;
  protected $sub_family;
  protected $style;
  protected $weight;
  protected $fixed_width;
  protected $fixed_sizes;
  protected $copyright;
  protected $license;
  protected $license_url;
  protected $version;
  protected $vendor;
  protected $vendor_url;
  protected $designer;
  protected $designer_url;
  protected $glyph_count;
  protected $character_count;

  protected $orthographies;
  

  function __construct($font_id = NULL)
  {
    $this->table = 'oflb_fonts';
    if($font_id != NULL)
    {
      parent::__construct($font_id);
    }
    
  }
  
  function import_fontaine($fontaine)
  {
    $this->common_name = mysql_real_escape_string($fontaine['commonName']);
    $this->native_name = mysql_real_escape_string($fontaine['nativeName']);
    $this->sub_family = mysql_real_escape_string($fontaine['subFamily']);
    $this->style = mysql_real_escape_string($fontaine['style']);
    $this->weight = mysql_real_escape_string($fontaine['weight']);
    $this->fixed_width = (strcasecmp($fontaine['fixedWidth'], 'yes') === 0);
    $this->fixed_sizes = (strcasecmp($fontaine['fixedSizes'], 'yes') === 0);
    $this->copyright = mysql_real_escape_string($fontaine['copyright']);
    $this->license = mysql_real_escape_string($fontaine['license']);
    $this->license_url = mysql_real_escape_string($fontaine['licenseUrl']);
    $this->version = mysql_real_escape_string($fontaine['version']);
    $this->vendor = mysql_real_escape_string($fontaine['vendor']);
    $this->vendor_url = mysql_real_escape_string($fontaine['vendorUrl']);
    $this->designer = mysql_real_escape_string($fontaine['designer']);
    $this->designer_url = mysql_real_escape_string($fontaine['designerUrl']);
    $this->glyph_count = mysql_real_escape_string($fontaine['glyphCount']);
    $this->character_count = mysql_real_escape_string($fontaine['characterCount']);
    
    $orthographies = $fontaine['orthographies'];
    
    $this->orthographies = array();
    
    foreach($orthographies as $ortho) {
      $orthography = new Orthography();

      $orthography->set_common_name(mysql_real_escape_string($ortho['commonName']));
      $orthography->set_native_name(mysql_real_escape_string($ortho['nativeName']));
      $orthography->set_support_level(mysql_real_escape_string(ucwords($ortho['supportLevel'])));
      
      if(isset($ortho['percentCoverage']))
      {
		$orthography->set_percent_coverage(mysql_real_escape_string($ortho['percentCoverage']));
 		$orthography->set_missing_values(mysql_real_escape_string($ortho['missingValues']));
	  }
      
      array_push($this->orthographies, $orthography);
    }
                
    $fontface = $this->common_name . $this->sub_family;
    $fontface = preg_replace('/[^\\pL0-9]+/u', '', $fontface);
    $fontface = iconv("utf-8", "us-ascii//TRANSLIT", $fontface);
    $fontface = preg_replace('/[^-a-z0-9]+/i', '', $fontface);
    
    $this->fontface_family = $fontface;
  }
  
  function save()
  {
    global $db;
    global $log;
    
    if(isset($this->id))
    {
    
    }
    else
    {
      $db->query("INSERT INTO oflb_fonts (commit_id, family_id, common_name, native_name, sub_family, style, weight, fixed_width, fixed_size, copyright, license, license_url, glyph_count, character_count, vendor, vendor_url, designer, designer_url, font_hash, file_directory, file_name, format_id, fontface_family, sample) VALUES ('$this->commit_id', '$this->family_id', '$this->common_name', '$this->native_name', '$this->sub_family', '$this->style', '$this->weight', '$this->fixed_width', '$this->fixed_sizes', '$this->copyright', '$this->license', '$this->license_url', '$this->glyph_count', '$this->character_count', '$this->vendor', '$this->vendor_url', '$this->designer', '$this->designer_url', '$this->font_hash', '$this->file_directory', '$this->file_name', (SELECT id FROM oflb_formats WHERE extension = '$this->file_extension'), '$this->fontface_family', '')");
      
      $this->id = mysql_insert_id();
      
      foreach($this->orthographies as $orthography)
      {
        $orthography->set_font_id($this->id);
        $orthography->save();
      }
    }
  
  }
  
  function get_license()
  {
    return $this->license;
  }
  
  function get_hash()
  {
    return $this->font_hash;
  }
  
  function get_fontface_family()
  {
    return $this->fontface_family;
  }
  
  function get_file_directory()
  {
    return $this->file_directory;
  }
  
  function get_file_name()
  {
    return $this->file_name;
  }
  
  function get_extension()
  {
    return $this->file_extension;
  }
  
  function set_commit_id($commit_id)
  {
     $this->commit_id = $commit_id;
  }
  
  function set_family_id($family_id)
  {
    $this->family_id = $family_id;
  }
  
  function set_font_hash($font_hash)
  {
    $this->font_hash = $font_hash;
  }
  
  function set_path_info($path_info)
  {
    $this->file_directory = $path_info['dirname'];
    $this->file_extension = strtolower($path_info['extension']);
    $this->file_name = $path_info['filename'];
    
    if($this->file_directory == '.') { $this->file_directory = ''; }
  }
  
}
  
?>
