<?php 

/** --------------------------------------------------------------

  @name     orthography.php
  @desc     Orthography class for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class Orthography extends Model
{
  protected $font_id;
  protected $common_name;
  protected $native_name;
  protected $support_level;
  protected $percent_coverage;
  protected $missing_values;

  function __construct($glyphs_id = NULL)
  {
    $this->table = 'oflb_glyphs';
    if($glyphs_id != NULL)
    {
      parent::__construct($glyphs_id);
    }
    
  }
  
  function save()
  {
    global $db;
  
    if($this->support_level == 'Full' || $this->support_level == 'Partial') 
    {                   
      $db->query("INSERT INTO oflb_glyphs (font_id, orthography_id, support_level_id, percent_coverage, missing_values) VALUES ('$this->font_id', (SELECT id from oflb_orthographies WHERE common_name = '$this->common_name'), (SELECT id from oflb_levels WHERE support_level = '$this->support_level'), '$this->percent_coverage', '$this->missing_values');");
      
      $this->id = mysql_insert_id();
      return mysql_insert_id();
    }
  
  }
  
  function set_font_id($font_id)
  {
    $this->font_id = $font_id;
  }
  
  function set_common_name($common_name)
  {
    $this->common_name = $common_name;
  }
  
  function set_native_name($native_name)
  {
    $this->native_name = $native_name;
  }
  
  function set_support_level($support_level)
  {
    $this->support_level = $support_level;
  }
  
  function set_percent_coverage($percent_coverage)
  {
    $this->percent_coverage = $percent_coverage;
  }
  
  function set_missing_values($missing_values)
  {
    $this->missing_values = $missing_values;
  }
      
}
  
?>
