<?php 

/** --------------------------------------------------------------

  @name     user.php
  @desc     User class for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class User extends Model
{

  protected $username;
  protected $full_name;
  protected $password;
  protected $usergroup;
  protected $email;
  protected $avatar;
  protected $homepage;
  protected $description;

  function __construct($userid = NULL)
  { 
    $this->table = 'aiki_users';
    $usergroup = '5';
    if($userid != NULL)
    {
      parent::__construct($userid);
    }
  }
  
  protected function select() {
    global $db;
    return $db->get_results("SELECT * FROM $this->table WHERE userid = '$this->id'");
  }
  
  function create($username, $full_name, $email, $password)
  {
    $this->username = $username;
    $this->full_name = $full_name;
    $this->email = $email;
    $this->password = $this->generate_password($password);
  }
  
  function load()
  {
    $results = $this->select();
    $user = $results[0];
    $this->username = $user->username;
    $this->full_name = $user->full_name;
    $this->email = $user->email;
    $this->password = $user->password;
    $this->usergroup = $user->usergroup;
  }
  
  function save()
  {
    global $db;
    if(isset($this->id))
    {
      $db->query("UPDATE $this->table SET full_name = '$this->full_name', email = '$this->email', password = '$this->password', usergroup='$this->usergroup' WHERE userid = '$this->id'");
    }
    else
    {
      $db->query("INSERT into $this->table (username, full_name, email, password, usergroup) VALUES ('$this->username', '$this->full_name', '$this->email', '$this->password', '$this->usergroup')");
      
      $this->id = mysql_insert_id();
      return mysql_insert_id();
    }
  }
  
  function set_full_name($full_name)
  {
    $this->full_name = $full_name;
  }
  
  function set_email($email)
  {
    $this->email = $email;
  }
  
  function set_password($password)
  {
    $this->password = $this->generate_password($password);
  }
  
  function set_usergroup($usergroup)
  {
    $this->usergroup = $usergroup;
  }
  
  public static function get_userid_by_username($username)
  {
    global $db;
    return $db->get_var("SELECT userid FROM aiki_users WHERE username = '$username'");
  }
  
  protected function generate_password($password)
  {
    return md5(md5($password));
  }

}

?>
