<?php

/* Globals
-------------------------------------------------------------- */
global $membership;
global $config;
global $db;
global $log;

/* Configuration Variables
-------------------------------------------------------------- */

define('USERID', $membership->userid);
define('USERNAME', $membership->username);
define('USERGROUP', $membership->group_level);

if(USERGROUP < 5)
{
  define('IS_LIBRARIAN', true);
}
else
{
  define('IS_LIBRARIAN', false);
}

if(USERGROUP < 4)
{
  define('IS_SECRETARY', true);
}
else
{
  define('IS_SECRETARY', false);
}

if(USERGROUP == 1)
{
  define('IS_ADMIN', true);
}
else
{
  define('IS_ADMIN', false);
}

$asset_path = AIKIROOT . DS . 'assets';
$asset_path_group_info = posix_getgrgid(filegroup($asset_path));

define('UPLOAD_PATH', $asset_path . DS . 'uploads' . DS . USERNAME);
define('FONTS_PATH', $asset_path . DS . 'fonts');
define('DOWNLOAD_PATH', $asset_path . DS . 'downloads');

define('FILEGROUP', $asset_path_group_info['name']);

define('SITE_URL', $config['url']);

define('FONTAINE', ROOT . DS . 'scripts' . DS . 'fontaine');

?>
