<?php	

/* Requires
-------------------------------------------------------------- */

define('AIKIROOT', dirname(dirname(dirname(dirname(dirname(__FILE__))))));

require_once AIKIROOT . DS . 'aiki.php';
require_once ROOT . DS . 'library' . DS . 'shared.php';
require_once ROOT . DS . 'config' . DS . 'config.php';

if(!USERID || USERGROUP > 5)
{
  die('You do not have permission to access this script.');
}

?>
