<?php 

/** --------------------------------------------------------------

  @name     model.php
  @desc     Model for Open Font Library.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

class Model {
  protected $table;
  protected $id;
  
  function __construct($record_id) {
    $this->id = $record_id;
  }
  
  function get_id() {
    return $this->id;
  }
  
  protected function select() {
    global $db;
    return $db->get_results("SELECT * FROM $this->table WHERE id = '$this->id'");
  }
}

?>
