<?php

/* Requires
-------------------------------------------------------------- */

require_once ROOT . DS . 'library' . DS . 'model.class.php';
require_once ROOT . DS . 'application' . DS . 'models' . DS . 'user.php';
require_once ROOT . DS . 'application' . DS . 'models' . DS . 'family.php';
require_once ROOT . DS . 'application' . DS . 'models' . DS . 'commit.php';
require_once ROOT . DS . 'application' . DS . 'models' . DS . 'font.php';
require_once ROOT . DS . 'application' . DS . 'models' . DS . 'orthography.php';

function sanitize($data) {
    return mysql_real_escape_string(preg_replace("/[[:blank:]]+/"," ", strip_tags(trim($data))));
}

function ensure_directory($directory)
{

  if(!is_dir($directory))
  {
    mkdir($directory, 0775, true);
  }
  
  ensure_ownership($directory);
 
}

function ensure_ownership($file)
{
  chmod($file, 0775);
  chgrp($file, FILEGROUP);
}

function destroy_directory($directory)
{

  $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory), RecursiveIteratorIterator::CHILD_FIRST);

  foreach ($iterator as $path)
  {
    if ($path->isDir())
    {
      chmod($path->__toString(), 0775);
      rmdir($path->__toString());
    }
    else {
      chmod($path->__toString(), 0775);
      unlink($path->__toString());
    }
  }
  chmod($directory, 0775);
  rmdir($directory);
}

?>
