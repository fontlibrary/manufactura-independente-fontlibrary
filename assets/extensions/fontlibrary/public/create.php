<?php	

/* --------------------------------------------------------------

  @name     create.php
  @desc     Custom script for Open Font Library. Handles New Font Uploads.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

/* Requires
-------------------------------------------------------------- */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

require_once ROOT . DS . 'library' . DS . 'bootstrap.php';

$message_heading = "";
$message_body = "";

//Check that the necessary values are being posted

if (!isset($_POST["family_name"]) || !isset($_FILES['zipfile']) || $_FILES['zipfile']['error'])
{
  $message_heading = 'Submission Rejected';
  $message_body = "We were not able to create your font. Make sure that your submission is correctly formatted and not too big.";
}
else
{
  $contributor_id = USERID;
      
  $category_id = sanitize($_POST['category']);
  $family_name = sanitize($_POST["family_name"]); 
  $description = sanitize($_POST["description"]);
    
  $family = new Family();
  $family->create($family_name, $description, $contributor_id, $category_id);
  
  if(IS_LIBRARIAN)
  {
        
      $contributor = NULL;

      if(isset($_POST['contributor']))
      {
          $contributor_name = sanitize($_POST['contributor']);
          $contributor_id = user::get_userid_by_username($contributor_name);
          if($contributor_id)
          {
              $contributor = new User($contributor_id);
          }
      }
      
        $family->set_contributor($contributor);
  }
  
  $project_name = $family->get_project_name();
  
  //Check if the project name is valid
  
  if($project_name == '' || $project_name == "-")
  {
      $message_heading = "Name Problem";
      $message_body = "The name $family_name is not a valid name for the font family. Please give it a name using Latin letters, as this will be used to set the URL for your font. You can change your font's display name later from its settings page.";
  }
  else
  {

    //Check if the project name is reserved
    
    $name_taken = $family->name_taken($family_name);
    
    if($name_taken)
    {
      $message_heading = "Sorry!";
      $message_body = "The Library already has a font called <strong>$family_name</strong>. Maybe you would like to <a href=\"" . SITE_URL . "font/" . $project_name . "\">view it</a> instead. If that is not the one you are trying to upload, please upload your font again with a new name.";
    }
    else
    {
      //Check that the upload is within the maximum size limits.
     
      $MAX_FILESIZE = ini_get('upload_max_filesize');

      if ($_FILES['zipfile']['size'] == 0 && $_SERVER['CONTENT_LENGTH'] > 0)
      {
        $message_heading = "Submission Rejected";
        $message_body = '<p>We cannot accept your submission. We can only accept files up to ' . $MAX_FILESIZE . '.</p>';
      }
      else
      {
        //Create the necessary folders and files.
        
        ensure_directory(UPLOAD_PATH);
        
        $zip_hash = md5_file($_FILES['zipfile']['tmp_name']);
        $uploaded_archive = UPLOAD_PATH . DS . $zip_hash . '.zip';
        $extracted_archive = UPLOAD_PATH . DS . $zip_hash;
        
        //Move the file into the Member's directory.
        
        if(!move_uploaded_file($_FILES['zipfile']['tmp_name'], $uploaded_archive))
        {
          $message_heading = "Submission Error";
          $message_body = "<p>There was an error uploading the file, please try again!</p>";
        }
        else
        {
          ensure_ownership($uploaded_archive);
              
          exec("/usr/bin/file -bi $uploaded_archive", $mime_info);
          $mimes = explode(';',$mime_info[0]);
          $mime_type = array_shift($mimes);
          
          if($mime_type != 'application/zip')
          {
            $message_heading = "Submission Rejected";
            $message_body = "<p>The file you are trying to upload is not a ZIP! ($mime_type)</p>";
          }
          else
          {
            ensure_directory($extracted_archive);
            
            $download_archive = $project_name.'.zip';//$_FILES['zipfile']['name'];
            
            //Look for fonts.
            
            exec("zipinfo -1 $uploaded_archive | grep -i -E '\.(otf|ttf)$'", $uploaded_fontlist);
            
            if(empty($uploaded_fontlist)) {
              $message_heading = "Submission Rejected";
              $message_body = "Your upload did not contain any OpenType or TrueType font files that we could find!";
            }
            else
            {
              //Extract the archive
              
              $has_fonts = 0;
              $unknown_licenses = 0;
            
              $uploaded_fontpaths = implode(" ", array_map("escapeshellarg", $uploaded_fontlist));
              
              exec("unzip $uploaded_archive $uploaded_fontpaths -d $extracted_archive", $out);
              
              //Process the fonts with Fontaine.
              
              $paths_and_fonts = array();
              
              foreach($uploaded_fontlist as $uploaded_font)
              {
                $uploaded_fontfile = $extracted_archive . DS . $uploaded_font;
                
                $fontaine_out = array();
                
                exec(FONTAINE . ' ' . escapeshellarg($uploaded_fontfile), $fontaine_out);
                
                $fontaine_string = implode($fontaine_out);
                
                $fontaine_json = json_decode($fontaine_string, true);
                
                if($fontaine_json) {
                  $has_fonts++;
                  $font = new Font();
                  $font->import_fontaine($fontaine_json['fonts'][0]);
                  
                  if($font->get_license() == 'Unknown or Proprietary License')
                  {
                    $unknown_licenses++;
                  }
                
                  $font->set_path_info(pathinfo($uploaded_font));
                  $font->set_font_hash(md5_file($uploaded_fontfile));
                  
                  $paths_and_fonts[$uploaded_fontfile] = $font;
                  
                }
              }
              
              if(!$has_fonts)
              {
                $message_heading = "Submission Problem";
                $message_body = "We found files marked as OpenType or TrueType but we were unable to read their contents.";
              }
              elseif($unknown_licenses)
              {
                $message_heading = "Submission Rejected";
                $message_body = "<strong>License Check:</strong> One or more of your fonts did not have a license that we recognize or support. The Open Font Library is a home for quality open fonts. We want all fonts to have a license included inside each font, so it is clear to everyone what their rights are. <em>You must add metadata to the font embedding a licence that we accept.</em> We recommend: The <a href=\"http://scripts.sil.org/OFL\">SIL Open Font License</a>, the <a href=\"http://www.gnu.org/copyleft/gpl.html\">GNU GPL v3</a> with Font Exception, or the <a href=\"http://creativecommons.org/publicdomain/zero/1.0/\">Creative Commons CC-0</a> license. Our wiki has more <a href=\"http://openfontlibrary.org/wiki/License\">information about these licenses</a>, and we hope you will read our <a href=\"http://openfontlibrary.org/wiki/License#Uploads\">Uploading documentation</a> and try again.";
              }
              else
              {
              
                $family_id = $family->save();
                
                $commit_date = $family->get_created_at();
                $download_archive = $project_name.'.zip';//$_FILES['zipfile']['name'];
                
                $commit = new Commit();
                $commit->create($family_id, $zip_hash, $commit_date, $download_archive);
                $commit_id = $commit->save();
                
                ensure_directory(FONTS_PATH . DS . $project_name);
                ensure_directory(FONTS_PATH . DS . $project_name . DS . $zip_hash);
                
                foreach($paths_and_fonts as $path => $font)
                {
                  $font->set_family_id($family_id);
                  $font->set_commit_id($commit_id);
                  $font->save();
                  
                  $font_directory = FONTS_PATH . DS . $project_name . DS . $zip_hash . DS . $font->get_hash();
                  
                  ensure_directory($font_directory);
                  
                  $font_file = $font_directory . DS . $font->get_fontface_family() . '.' . $font->get_extension();
                  
                  $uploaded_fontfile = $path;
                  
                  rename($uploaded_fontfile, $font_file);
                  
                  ensure_ownership($font_file);
                }
                
                $family->update_license();
                
                ensure_directory(DOWNLOAD_PATH . DS . $project_name);
                    
                $download_directory = DOWNLOAD_PATH . DS . $project_name . DS . $zip_hash;
                
                ensure_directory($download_directory);
                
                rename($uploaded_archive, $download_directory. DS . $download_archive);
                 
                $message_heading = 'Submission Accepted';
                $message_body = "Your font has been created! You can <a href=\"" . SITE_URL . "font/" . $project_name . "\">view  $family_name</a> on its new font profile page.";
              
              }
            }
            
            destroy_directory($extracted_archive);
            
          }
          
        }
        
      }
      
    }
  }
  
}

$html = "<div class=\"element\"><ul class=\"primary\"><li class=\"heading first\">" . $message_heading . "</li></ul><div class=\"spec text\"><p>" . $message_body . "</p></div></div><div class=\"element\"><ul class=\"primary\"><li class=\"heading first\">Keep Creating</li></ul><div class=\"spec text\"><p>If you want to create another font, please head back to the <a href=\"create\">upload page</a>.</p></div></div>";

echo $html;

?>
