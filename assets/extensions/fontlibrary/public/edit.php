<?php	

/* --------------------------------------------------------------

  @name     edit.php
  @desc     Custom script for Open Font Library. Handles Edits to Families.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

/* Requires
-------------------------------------------------------------- */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

require_once ROOT . DS . 'library' . DS . 'bootstrap.php';

$message = '';

//Check that family and contributor id are set

if(!isset($_POST["record_id"]) || !isset($_POST["contributor_id"]))
{
  $message = 'You have not specified a font to edit.';
}
else
{
  //Check that family and contributor id are set
  
  if(!isset($_POST["family_name"]) || !isset($_POST['description']))
  {
    $message = "You must supply a family name and description.";
  }
  else
  {
    $family_id = sanitize($_POST["record_id"]);
    $contributor_id = sanitize($_POST["contributor_id"]);
    $status = sanitize($_POST["status"]);
    $category_id = sanitize($_POST['category']);
    $sample = sanitize($_POST['sample']);
    
    $family_name = sanitize($_POST["family_name"]); 
    $description = sanitize($_POST["description"]);
    
    $family = new Family($family_id);
    $family->load();
    
    //Check if the project name is reserved
    
    $name_taken = $family->name_taken($family_name);
    
    if($name_taken)
    {
      $message = "The Library already has a font with that name!";
    }
    else
    {
      $original_contributor = $family->get_contributor_id();
      
      //Check that the user has permission to edit this font
      
      if($original_contributor != $contributor_id && !IS_LIBRARIAN)
      {
        $message = "You do not have permission to edit this font.";
      }
      else
      {
        //FIXME Check that the category is valid
        $family->set_category_id($category_id);
        $family->set_family_name($family_name);
        $family->set_description($description);
        $family->set_sample($sample);
        
        //Librarians are allowed to manually specify a contributor and set font status
        
        if(IS_LIBRARIAN)
        {
            
          $contributor = NULL;
        
          if(isset($_POST['contributor']))
          {
              $contributor_name = sanitize($_POST['contributor']);
              $contributor_id = user::get_userid_by_username($contributor_name);
              if($contributor_id)
              {
                  $contributor = new User($contributor_id);
              }
          }
          
            $family->set_contributor($contributor);
            $family->set_status($status);
        }
        
        $family->save();
        $message = 'Your font has been saved!';
      }
    }
  }
}

echo '<h3>' . $message . '</h3>';

?>
