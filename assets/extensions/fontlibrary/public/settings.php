<?php	

/* --------------------------------------------------------------

  @name     settiongs.php
  @desc     Custom script for Open Font Library. Handles User Settings.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

/* Requires
-------------------------------------------------------------- */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

require_once ROOT . DS . 'library' . DS . 'bootstrap.php';

$message = '';

//Check that user id is set

if(!isset($_POST["record_id"]))
{
  $message = 'You have not specified a user to edit.';
}
else
{
  $userid = $_POST["record_id"];
  
  //Check that the user has permission to edit these settings

  if(USERID != $userid && !IS_SECRETARY)
  {
    $message = 'You do not have permission to edit these settings.';
  }

  //Check that name and email are set
  
  if(!isset($_POST['email']) || !isset($_POST['full_name']))
  {
    $message = 'You must supply an email address and your full name.';
  }
  else
  {
    $user = new User($userid);
    $user->load();
    
    $email = sanitize($_POST["email"]);
    $full_name = sanitize($_POST["full_name"]);
    
    $user->set_email($email);
    $user->set_full_name($full_name);
    
    if(isset($_POST["usergroup"]) && IS_ADMIN)
    {
        $usergroup = sanitize($_POST["usergroup"]);
        $user->set_usergroup($usergroup);
    }
    
    if(isset($_POST["password"]) && (strlen($_POST["password"]) > 0))
    {
      $user->set_password($_POST["password"]);
      $message = 'Your password has been changed. ';
    }
    
    $user->save();
    $message .= 'Your settings have been saved!';
  }
}

echo '<h3>' . $message . '</h3>';

?>
