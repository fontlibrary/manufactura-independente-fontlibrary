<?php	

/* --------------------------------------------------------------

  @name     update.php
  @desc     Custom script for Open Font Library. Handles Font Updates.
  @author   Fabricatorz, LLC
  @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html
  
-------------------------------------------------------------- */

/* Requires
-------------------------------------------------------------- */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

require_once ROOT . DS . 'library' . DS . 'bootstrap.php';

$message_heading = "";
$message_body = "";

//Check that the necessary values are being posted

if(!isset($_POST["record_id"]))
{
    $message_heading = "Upload Error";
    $message_body = 'You have not specified a font to edit.';
}
else
{
    if (!isset($_FILES['zipfile']) || $_FILES['zipfile']['error'])
    {
      $message_heading = 'Update Rejected';
      $message_body = "We were not able to update your font. Make sure that your submission is correctly formatted and not too big.";
      
  $message_body = "<p>We were not able to create your font. Make sure that your submission is correctly formatted. $error</p>";
    }
    else
    {
      $family_id = sanitize($_POST["record_id"]);
          
      $family = new Family($family_id);
      $family->load();
      
      $family_name = $family->get_family_name();
      $project_name = $family->get_project_name();
      $contributor_id = $family->get_contributor_id();
      
      if($contributor_id != USERID && !IS_LIBRARIAN)
      {
        $message = "You do not have permission to edit this font.";
      }
      else
      {
          //Check that the upload is within the maximum size limits.
         
          $MAX_FILESIZE = ini_get('upload_max_filesize');

          if ($_FILES['zipfile']['size'] == 0 && $_SERVER['CONTENT_LENGTH'] > 0)
          {
            $message_heading = "Submission Rejected";
            $message_body = '<p>We cannot accept your update. We can only accept files up to ' . $MAX_FILESIZE . '.</p>';
          }
          else
          {
            //Create the necessary folders and files.
            
            ensure_directory(UPLOAD_PATH);
            
            $zip_hash = md5_file($_FILES['zipfile']['tmp_name']);
            $uploaded_archive = UPLOAD_PATH . DS . $zip_hash . '.zip';
            $extracted_archive = UPLOAD_PATH . DS . $zip_hash;
            
            //Move the file into the Member's directory.
            
            if(!move_uploaded_file($_FILES['zipfile']['tmp_name'], $uploaded_archive))
            {
              $message_heading = "Submission Error";
              $message_body = "<p>There was an error uploading the file, please try again!</p>";
            }
            else
            {
              ensure_ownership($uploaded_archive);
                  
              exec("/usr/bin/file -bi $uploaded_archive", $mime_info);
              $mimes = explode(';',$mime_info[0]);
              $mime_type = array_shift($mimes);
              
              if($mime_type != 'application/zip')
              {
                $message_heading = "Submission Rejected";
                $message_body = "<p>The file you are trying to upload is not a ZIP! ($mime_type)</p>";
              }
              else
              {
                ensure_directory($extracted_archive);
                
                $download_archive = $project_name.'.zip';//$_FILES['zipfile']['name'];
                
                //Look for fonts.
                
                exec("zipinfo -1 $uploaded_archive | grep -i -E '\.(otf|ttf)$'", $uploaded_fontlist);
                
                if(empty($uploaded_fontlist)) {
                  $message_heading = "Submission Rejected";
                  $message_body = "Your upload did not contain any OpenType or TrueType font files that we could find!";
                }
                else
                {
                  //Extract the archive
                  
                  $has_fonts = 0;
                  $unknown_licenses = 0;
                
                  $uploaded_fontpaths = implode(" ", array_map("escapeshellarg", $uploaded_fontlist));
                  
                  exec("unzip $uploaded_archive $uploaded_fontpaths -d $extracted_archive", $out);
                  
                  //Process the fonts with Fontaine.
                  
                  $paths_and_fonts = array();
                  
                  foreach($uploaded_fontlist as $uploaded_font)
                  {
                    $uploaded_fontfile = $extracted_archive . DS . $uploaded_font;
                    
                    $fontaine_out = array();
                    
                    exec(FONTAINE . ' ' . escapeshellarg($uploaded_fontfile), $fontaine_out);
                    
                    $fontaine_string = implode($fontaine_out);
                    
                    $fontaine_json = json_decode($fontaine_string, true);
                    
                    if($fontaine_json) {
                      $has_fonts++;
                      $font = new Font();
                      $font->import_fontaine($fontaine_json['fonts'][0]);
                      
                      if($font->get_license() == 'Unknown or Proprietary License')
                      {
                        $unknown_licenses++;
                      }
                    
                      $font->set_path_info(pathinfo($uploaded_font));
                      $font->set_font_hash(md5_file($uploaded_fontfile));
                      
                      $paths_and_fonts[$uploaded_fontfile] = $font;
                      
                    }
                  }
                  
                  if(!$has_fonts)
                  {
                    $message_heading = "Submission Problem";
                    $message_body = "We found files marked as OpenType or TrueType but we were unable to read their contents.";
                  }
                  elseif($unknown_licenses)
                  {
                    $message_heading = "Update Rejected";
                    $message_body = "<strong>License Check:</strong> At least one of the fonts in your update does not have a license that we recognize or support. <em>You must add metadata to all fonts embedding a licence that we accept.</em> Please correct your font files and try again.";
                  }
                  else
                  {
                    
                    $commit_date = time();
                    $download_archive = $project_name.'.zip';//$_FILES['zipfile']['name'];
                    
                    $commit = new Commit();
                    $commit->create($family_id, $zip_hash, $commit_date, $download_archive);
                    $commit_id = $commit->save();
                    
                    ensure_directory(FONTS_PATH . DS . $project_name);
                    ensure_directory(FONTS_PATH . DS . $project_name . DS . $zip_hash);
                    
                    foreach($paths_and_fonts as $path => $font)
                    {
                      $font->set_family_id($family_id);
                      $font->set_commit_id($commit_id);
                      $font->save();
                      
                      $font_directory = FONTS_PATH . DS . $project_name . DS . $zip_hash . DS . $font->get_hash();
                      
                      ensure_directory($font_directory);
                      
                      $font_file = $font_directory . DS . $font->get_fontface_family() . '.' . $font->get_extension();
                      
                      $uploaded_fontfile = $path;
                      
                      rename($uploaded_fontfile, $font_file);
                  
                      ensure_ownership($font_file);      
                    }
                    
                    $family->save();
                    $family->update_license();
                    
                    ensure_directory(DOWNLOAD_PATH . DS . $project_name);
                        
                    $download_directory = DOWNLOAD_PATH . DS . $project_name . DS . $zip_hash;
                    
                    ensure_directory($download_directory);
                    
                    rename($uploaded_archive, $download_directory. DS . $download_archive);
                    
                    ensure_ownership($download_directory. DS . $download_archive);
                     
                    $message_heading = 'Update Successful';
                    $message_body = "Your font has been updated! You can <a href=\"" . SITE_URL . "font/" . $project_name . "\">view  $family_name</a> on its font profile page.";
                  
                  }
                }
                
                destroy_directory($extracted_archive);
                
              }
              
            }
            
          }
          
      }
      
    }
}

$html = "<div class=\"element\"><ul class=\"primary\"><li class=\"heading first\">" . $message_heading . "</li></ul><div class=\"spec text\"><p>" . $message_body . "</p></div></div>";

echo $html;

?>
