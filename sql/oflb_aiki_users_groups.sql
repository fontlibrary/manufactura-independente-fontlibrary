--
-- Table structure for table `aiki_users_groups`
--

DROP TABLE IF EXISTS `aiki_users_groups`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `aiki_users_groups` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_permissions` varchar(255) NOT NULL,
  `group_level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `aiki_users_groups`
--

LOCK TABLES `aiki_users_groups` WRITE;
/*!40000 ALTER TABLE `aiki_users_groups` DISABLE KEYS */;
INSERT INTO `aiki_users_groups` VALUES (1,0,'System Administrators','SystemGOD',1),(2,0,'Modules Administrators','ModulesGOD',2),(3,0,'Guests','ViewPublished',100),(4,0,'Banned users','ViewPublished',101),(5,0,'Members','member',5),(6,0,'Secretaries','secretary',3),(7,0,'Librarians','librarian',4);
/*!40000 ALTER TABLE `aiki_users_groups` ENABLE KEYS */;
UNLOCK TABLES;
