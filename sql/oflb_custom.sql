--
-- Table structure for table `oflb_categories`
--

DROP TABLE IF EXISTS `oflb_categories`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_categories` (
  `id` smallint(9) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(24) NOT NULL,
  `tag` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_categories`
--

LOCK TABLES `oflb_categories` WRITE;
/*!40000 ALTER TABLE `oflb_categories` DISABLE KEYS */;
INSERT INTO `oflb_categories` VALUES (5,'Serif','serif'),(11,'Sans-serif','sans-serif'),(17,'Monospaced','monospaced'),(23,'Handwriting','handwriting'),(29,'Display','display'),(35,'Dingbat','dingbat'),(41,'Blackletter','blackletter');
/*!40000 ALTER TABLE `oflb_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_commits`
--

DROP TABLE IF EXISTS `oflb_commits`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_commits` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `family_id` int(11) unsigned NOT NULL,
  `hash` varchar(40) NOT NULL,
  `commit_date` datetime NOT NULL,
  `download` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_commits`
--

LOCK TABLES `oflb_commits` WRITE;
/*!40000 ALTER TABLE `oflb_commits` DISABLE KEYS */;
/*!40000 ALTER TABLE `oflb_commits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_families`
--

DROP TABLE IF EXISTS `oflb_families`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_families` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contributor_id` int(11) unsigned NOT NULL,
  `family_name` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `changed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remote_repository` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `license_id` tinyint(3) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `common_name` (`family_name`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_families`
--

LOCK TABLES `oflb_families` WRITE;
/*!40000 ALTER TABLE `oflb_families` DISABLE KEYS */;
/*!40000 ALTER TABLE `oflb_families` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_fonts`
--

DROP TABLE IF EXISTS `oflb_fonts`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_fonts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commit_id` int(11) unsigned NOT NULL,
  `family_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `common_name` varchar(255) NOT NULL,
  `native_name` varchar(255) NOT NULL,
  `sub_family` varchar(25) NOT NULL,
  `style` varchar(25) NOT NULL,
  `weight` varchar(25) NOT NULL,
  `fixed_width` tinyint(1) NOT NULL,
  `fixed_size` tinyint(1) NOT NULL,
  `copyright` text NOT NULL,
  `license` text NOT NULL,
  `license_url` varchar(255) NOT NULL,
  `glyph_count` int(9) NOT NULL,
  `character_count` int(9) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `version` varchar(25) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `designer` varchar(255) NOT NULL,
  `vendor_url` varchar(255) NOT NULL,
  `designer_url` varchar(255) NOT NULL,
  `font_hash` varchar(40) NOT NULL,
  `file_directory` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `format_id` tinyint(9) unsigned NOT NULL,
  `fontface_family` varchar(255) NOT NULL,
  `sample` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_fonts`
--

LOCK TABLES `oflb_fonts` WRITE;
/*!40000 ALTER TABLE `oflb_fonts` DISABLE KEYS */;
/*!40000 ALTER TABLE `oflb_fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_formats`
--

DROP TABLE IF EXISTS `oflb_formats`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_formats` (
  `id` tinyint(3) unsigned NOT NULL,
  `format` varchar(255) NOT NULL,
  `extension` varchar(4) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_formats`
--

LOCK TABLES `oflb_formats` WRITE;
/*!40000 ALTER TABLE `oflb_formats` DISABLE KEYS */;
INSERT INTO `oflb_formats` VALUES (1,'opentype','otf','OpenType'),(2,'truetype','ttf','TrueType'),(3,'eot','eot','Embedded OpenType'),(4,'woff','woff','Web Open Font Format'),(5,'svg','svg','Scalable Vector Graphics');
/*!40000 ALTER TABLE `oflb_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_glyphs`
--

DROP TABLE IF EXISTS `oflb_glyphs`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_glyphs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `font_id` int(11) unsigned NOT NULL,
  `orthography_id` smallint(5) unsigned NOT NULL,
  `support_level_id` tinyint(3) unsigned NOT NULL,
  `percent_coverage` tinyint(3) unsigned DEFAULT NULL,
  `missing_values` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2999 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_glyphs`
--

LOCK TABLES `oflb_glyphs` WRITE;
/*!40000 ALTER TABLE `oflb_glyphs` DISABLE KEYS */;
/*!40000 ALTER TABLE `oflb_glyphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_levels`
--

DROP TABLE IF EXISTS `oflb_levels`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_levels` (
  `id` tinyint(3) unsigned NOT NULL,
  `support_level` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_levels`
--

LOCK TABLES `oflb_levels` WRITE;
/*!40000 ALTER TABLE `oflb_levels` DISABLE KEYS */;
INSERT INTO `oflb_levels` VALUES (1,'Full'),(2,'Partial'),(3,'Fragmentary'),(4,'None');
/*!40000 ALTER TABLE `oflb_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_licenses`
--

DROP TABLE IF EXISTS `oflb_licenses`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_licenses` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `license` varchar(127) NOT NULL,
  `license_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_licenses`
--

LOCK TABLES `oflb_licenses` WRITE;
/*!40000 ALTER TABLE `oflb_licenses` DISABLE KEYS */;
INSERT INTO `oflb_licenses` VALUES (5,'Aladdin Free Public License','http://pages.cs.wisc.edu/~ghost/doc/AFPL/6.01/Public.htm'),(11,'Apache 2.0','http://www.apache.org/licenses/LICENSE-2.0'),(17,'Arphic Public License','http://ftp.gnu.org/gnu/non-gnu/chinese-fonts-truetype/LICENSE'),(23,'Bitstream Vera License (and derivative projects)','http://www.gnome.org/fonts/'),(29,'CC-BY','http://creativecommons.org/licenses/by/3.0/'),(35,'CC-BY-SA','http://creativecommons.org/licenses/by-sa/3.0/'),(41,'CC-0','http://creativecommons.org/publicdomain/zero/1.0/'),(47,'Freeware',''),(53,'GNU General Public License','http://www.gnu.org/copyleft/gpl.html'),(59,'GPL with font exception','http://www.gnu.org/copyleft/gpl.html'),(65,'GUST Font License','http://tug.org/fonts/licenses/GUST-FONT-LICENSE.txt'),(71,'IPA','http://opensource.org/licenses/ipafont.html'),(77,'GNU Lesser General Public License','http://www.gnu.org/licenses/lgpl.html'),(83,'Magenta Open License','http://www.ellak.gr/fonts/mgopen/index.en.html'),(89,'MIT (X11) License','http://www.opensource.org/licenses/mit-license.php'),(95,'M+ Fonts Project License','http://mplus-fonts.sourceforge.jp/webfonts/index-en.html#license'),(101,'OFL (SIL Open Font License)','http://scripts.sil.org/OFL'),(107,'Public Domain (not a license)',''),(113,'STIX Font License (deprecated: the new version of the fonts have been released under the OFL)','http://www.aip.org/stixfonts/news.html'),(119,'Unknown or Proprietary License',''),(125,'License to TeX Users Group for the Utopia Typeface','http://tug.org/fonts/utopia/LICENSE-utopia.txt'),(131,'XFree86 License','http://www.xfree86.org/legal/licenses.html'),(137,'ParaType Free Font Licensing Agreement','http://www.paratype.com/public/pt_openlicense_eng.asp'),(143,'Ubuntu Font License','http://font.ubuntu.com/ufl/ubuntu-font-licence-1.0.txt');
/*!40000 ALTER TABLE `oflb_licenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_orthographies`
--

DROP TABLE IF EXISTS `oflb_orthographies`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_orthographies` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `common_name` varchar(255) NOT NULL,
  `native_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=474 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_orthographies`
--

LOCK TABLES `oflb_orthographies` WRITE;
/*!40000 ALTER TABLE `oflb_orthographies` DISABLE KEYS */;
INSERT INTO `oflb_orthographies` VALUES (5,'Afrikaans','Afrikaans'),(11,'Arabic','العربية'),(17,'Archaic Greek Letters',''),(23,'Armenian','Հայերեն'),(29,'Baltic',''),(35,'Basic Cyrillic','Кири́ллица'),(41,'Basic Greek','Ελληνικό αλφάβητο'),(47,'Basic Latin',''),(53,'Bengali','বাংলা'),(59,'Unified Canadian Aboriginal Syllabics',''),(65,'Catalan','Català'),(71,'Central European',''),(77,'Cherokee','ᏣᎳᎩ'),(83,'Chess Symbols',''),(89,'Claudian Letters',''),(95,'Coptic','Ⲙⲉⲧⲣⲉⲙ̀ⲛⲭⲏⲙⲓ'),(101,'Devanagari','देवनागरी'),(107,'Dutch','Nederlands'),(113,'Ethiopic','ግዕዝ\"'),(119,'Euro',''),(125,'Extended Arabic','العربية'),(131,'Farsi','فارسی'),(137,'Georgian','ქართული დამწერლობა'),(143,'Gujarati','ગુજરાતી લિપિ'),(149,'Gurmukhi','ਗੁਰਮੁਖੀ'),(155,'Korean Hangul','한글 / 조선글'),(161,'Hanunó\'o',''),(167,'Hebrew','עִבְרִית'),(173,'Hong Kong Supplementary Character Set','香港增補字符集'),(179,'Igbo Onwu','Asụsụ Igbo'),(185,'IPA','aɪ pʰiː eɪ'),(191,'Japanese Jinmeiyo','日本人名用漢字'),(197,'Joyo','日本常用漢字'),(203,'Kana','仮名'),(209,'Kannada','ಕನ್ನಡ'),(215,'Kazakh','قازاق'),(221,'Khmer','អក្សរខ្មែរ'),(227,'Japanese Kokuji','日本国字'),(233,'Lao','ພາສາລາວ'),(239,'Latin Ligatures',''),(245,'Malayalam','മലയാളം'),(251,'Mathematical Operators',''),(257,'Mongolian',''),(263,'Myanmar','မြန်မာအက္ခရာ'),(269,'New Tai Lue',''),(275,'N’Ko','ߒߞߏ'),(281,'Ogham',''),(287,'Oriya','ଓଡ଼ିଆ'),(293,'Osmanya',''),(299,'Pan African Latin',''),(305,'Pashto','پښتو'),(311,'Phags','Pa'),(317,'Pinyin','汉语拼音'),(323,'Polytonic Greek',''),(329,'Romanian','Română'),(335,'Runic','ᚠᚢᚦᛆᚱᚴ'),(341,'Simplified Chinese','中文简体字'),(347,'Sindhi','سنڌي'),(353,'Sinhala','සිංහල'),(359,'Siraiki',''),(365,'Syriac','ܠܫܢܐ ܣܘܪܝܝܐ'),(371,'Tai Le',''),(377,'Tai Tham (Lanna)','ᨲᩫ᩠ᩅᨾᩮᩥᩬᨦ'),(383,'Tamil','தமிழ் அரிச்சுவடி'),(389,'Telugu','తెలుగు'),(395,'Thaana','ތާނަ'),(401,'Thai','ภาษาไทย'),(407,'Tibetan','དབུ་ཅན་'),(413,'Tifinagh','ⵜⵉⴼⵉⵏⴰⵖ'),(419,'Traditional Chinese','中文正體字'),(425,'Turkish','Türkçe'),(431,'Uighur','ئۇيغۇر'),(437,'Urdu','اُردو'),(443,'Vai',''),(449,'Venda Tshivenḓa',''),(455,'Vietnamese','tiếng Việt'),(461,'Western European',''),(467,'Yi','ꆈꌠꁱꂷ'),(473,'Chinese Zhuyin Fuhao','注音符號');
/*!40000 ALTER TABLE `oflb_orthographies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oflb_status`
--

DROP TABLE IF EXISTS `oflb_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `oflb_status` (
  `id` tinyint(4) NOT NULL,
  `status` varchar(20) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `oflb_status`
--

LOCK TABLES `oflb_status` WRITE;
/*!40000 ALTER TABLE `oflb_status` DISABLE KEYS */;
INSERT INTO `oflb_status` VALUES (-4,'Expunged'),(-3,'Flagged'),(-2,'Broken'),(-1,'Hidden'),(0,'Normal'),(1,'Featured');
/*!40000 ALTER TABLE `oflb_status` ENABLE KEYS */;
UNLOCK TABLES;
